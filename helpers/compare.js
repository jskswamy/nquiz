'use strict';

var _ = require('lodash');

exports.hash = function(x, y) {
  var that = this;
  return _.every(x, function(value, key) {
    if (Array.isArray(value)) {
      return that.array(value, y[key]);
    } else {
      return value === y[key];
    }
  });
};

exports.array = function(x, y) {
  return _.isEqual(_.sortBy(x), _.sortBy(y));
};
