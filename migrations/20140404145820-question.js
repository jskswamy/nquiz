"use strict";

var dbm = require('db-migrate')
  , type = dbm.dataType;

exports.up = function(db, callback) {
  db.createTable('questions', {
    id: { type: 'int', primaryKey: true, autoIncrement: true },
    text: 'string'
  }, callback);
};

exports.down = function(db, callback) {
  db.dropTable('questions', callback);
};
