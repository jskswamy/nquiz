"use strict";

var dbm = require('db-migrate')
  , type = dbm.dataType
  , async = require('async');

exports.up = function(db, callback) {
  async.series([
    db.addColumn.bind(db, 'answers', '"isAnswer"', {
      type: 'boolean',
      defaultValue: false
    }),
    db.removeColumn.bind(db, 'questions', '"answerId"')
  ], callback);
};

exports.down = function(db, callback) {
  async.series([
    db.addColumn.bind(db, 'questions','"answerId"', 'int'),
    db.removeColumn.bind(db, 'answers', '"isAnswer"')
  ], callback);
};
