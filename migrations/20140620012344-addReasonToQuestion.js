'use strict';

var dbm = require('db-migrate')
  , type = dbm.dataType;

exports.up = function(db, callback) {
  db.addColumn('questions', 'reason', 'text', callback);
};

exports.down = function(db, callback) {
  db.removeColumn('questions', 'reason, callback');
};
