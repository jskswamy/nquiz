'use strict';

var dbm = require('db-migrate')
  , type = dbm.dataType
  , async = require('async');

exports.up = function(db, callback) {
  async.series([
    db.createTable.bind(db, 'sessions', {
      id: { type: type.INT, primaryKey: true, autoIncrement: true },
      startDate: 'datetime',
      endDate: 'datetime',
      duration: 'int'
    }),
    db.createTable.bind(db, 'sessionCategories', {
      sessionId: 'int',
      categoryId: 'int'
    })
  ], callback);
};

exports.down = function(db, callback) {
  async.series([
    db.dropTable.bind(db, 'sessions'),
    db.dropTable.bind(db, 'sessionCategories')
  ], callback);
};
