'use strict';

var dbm = require('db-migrate')
  , type = dbm.dataType;

exports.up = function(db, callback) {
  db.addColumn('assessments','result', 'text', callback);
};

exports.down = function(db, callback) {
  db.removeColumn('assessments', 'result', callback);
};
