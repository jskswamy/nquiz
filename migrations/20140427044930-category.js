"use strict";

var dbm = require('db-migrate')
  , type = dbm.dataType
  , async = require('async');

exports.up = function(db, callback) {
  async.series([
    db.createTable.bind(db, 'categories', {
      id: { type: 'int', primaryKey: true, autoIncrement: true },
      name: 'string'
    }),
    db.createTable.bind(db, 'categoryQuestions', {
      categoryId: 'int',
      questionId: 'int'
    })
  ], callback);
};

exports.down = function(db, callback) {
  async.series([
    db.dropTable.bind(db, 'categories'),
    db.dropTable.bind(db, 'categoryQuestions')
  ], callback);
};
