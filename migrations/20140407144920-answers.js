"use strict";

var dbm = require('db-migrate')
  , type = dbm.dataType;

exports.up = function(db, callback) {
  db.createTable('answers', {
    id: { type: 'int', primaryKey: true, autoIncrement: true },
    questionId: 'int',
    text: 'string'
  }, callback);
};

exports.down = function(db, callback) {
  db.dropTable('answers', callback);
};
