'use strict';

var dbm = require('db-migrate')
  , type = dbm.dataType
  , async = require('async');

exports.up = function(db, callback) {
  async.series([
    db.addColumn.bind(db, 'sessions','"name"', 'string'),
    db.addColumn.bind(db, 'sessions','"noOfQuestions"', 'int')
  ], callback);
};

exports.down = function(db, callback) {
  async.series([
    db.removeColumn.bind(db, 'sessions','"name"'),
    db.removeColumn.bind(db, 'sessions','"noOfQuestions"')
  ], callback);
};
