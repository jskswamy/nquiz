'use strict';

var dbm = require('db-migrate')
  , type = dbm.dataType
  , async = require('async');

exports.up = function(db, callback) {
  async.series([
    db.changeColumn.bind(db, 'sessions', 'token', {primaryKey: true}),
    db.removeColumn.bind(db, 'sessions', 'id'),
  ], callback);
};

exports.down = function(db, callback) {
  async.series([
    db.addColumn.bind(db, 'sessions', 'id', {type: 'int'}),
    db.changeColumn.bind(db, 'sessions', 'token', {primaryKey: false})
  ], callback);
};
