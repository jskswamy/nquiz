'use strict';

var dbm = require('db-migrate');
var type = dbm.dataType;

exports.up = function(db, callback) {
  db.addColumn('sessions','token', 'string', callback);
};

exports.down = function(db, callback) {
  db.removeColumn('sessions','name', callback);
};
