
/**
 * Module dependencies.
 */

"use strict";

var express = require('express')
  , bodyParser = require('body-parser')
  , favicon = require('serve-favicon')
  , morgan = require('morgan')
  , errorHandler = require('errorhandler')
  , methodOverride = require('method-override')
  , passport = require('passport')
  , localAutentication = require('./config/passport')
  , http = require('http')
  , path = require('path')
  , session = require('cookie-session')
  , env = process.env.NODE_ENV || 'development'
  , route = require('./route')
  , userRoles = require('./config/authorization')()
  , compress = require('compression')
  , app = express()
  , flash = require('express-flash')
  , userSession = require('./middleware/session')
  , newrelic = require('newrelic');

app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(bodyParser());
app.use(methodOverride());
app.use(compress({
  threshold: 100
}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({secret: 'node quiz app'}));
app.use(flash());
app.use(passport.initialize());
app.use(userRoles.middleware());
app.use(passport.session());
app.use(userSession);

//app.use(favicon());
if (env === 'development') {
  app.use(errorHandler());
  app.use(morgan('dev'));
} else {
  app.use(morgan('tiny'));
}

localAutentication(passport);
route.bind(app, userRoles);

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
