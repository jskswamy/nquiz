'use strict';

var request = require('supertest')
  , express = require('express')
  , app = express()
  , route = require('../../../route')
  , chai = require('chai')
  , factory = require('../../factories')
  , helper = require('../../helper')
  , models = require('../../../models')
  , expect = chai.expect;

describe('Login controller', function() {

  var getTransaction = helper.bindWebApp(this, models, app);

  it('should be able to login', function(done) {
    var transaction = getTransaction();
    var userPassword = 'password';

    factory.createUser(transaction, userPassword)
    .then(function(user) {
      request(app).post('/login')
      .send({email: user.email, password: userPassword })
      .end(function(err, res) {
        expect(res).to.have.property('status', 302);
        expect(res.header).to.be.have.property('location', '/');
        done();
      });
    })
    .fail(function(err) {
      console.log(err);
      done();
    });
  });

  it('should not be able to login with wrong password', function(done) {
    var transaction = getTransaction();
    var userPassword = 'password';

    factory.createUser(transaction, userPassword)
    .then(function(user) {
      request(app).post('/login')
      .send({email: 'someotheruser@somthing.com', password: userPassword })
      .end(function(err, res) {
        expect(res).to.have.property('status', 302);
        expect(res.header).to.be.have.property('location', '/login');
        done();
      });
    });
  });

  it('should not be able to login with wrong username', function(done) {
    var transaction = getTransaction();
    var userPassword = 'password';

    factory.createUser(transaction, userPassword)
    .then(function(user) {
      request(app).post('/login')
      .send({email: user.email, password: 'wrongpassword' })
      .end(function(err, res) {
        expect(res).to.have.property('status', 302);
        expect(res.header).to.be.have.property('location', '/login');
        done();
      });
    });
  });

});
