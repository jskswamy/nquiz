'use strict';

var request = require('supertest')
  , express = require('express')
  , app = express()
  , route = require('../../../route')
  , chai = require('chai')
  , expect = chai.expect
  , factory = require('../../factories')
  , helper = require('../../helper')
  , models = require('../../../models')
  , User = models.User;

describe('User Controller', function() {
  var getTransaction = helper.bindWebApp(this, models, app);

  describe('create', function() {

    it('should be able to create a user', function(done) {
      var userEmail = 'userone@domain.com';
      var transaction = getTransaction();

      request(app)
      .post('/users')
      .send({email: userEmail, password: 'password', firstName: 'tom', lastName: 'cat'})
      .end(function(err, res) {
        User.findBy({where: { email: userEmail }}, {transaction: transaction})
        .then(function(user) {
          expect(res).to.have.property('status', 200);
          expect(res.body).to.have.property('id', user.id);
          expect(res.body).not.to.have.property('password');
          expect(res.body).to.have.property('firstName', 'tom');
          expect(res.body).to.have.property('lastName', 'cat');
          done();
        })
        .fail(function(err) {
          done(new Error(err));
        });
      });
    });


    it('should not be able to create a user without email', function(done) {
      request(app)
      .post('/users')
      .send({email: null, password: 'password', firstName: 'tom', lastName: 'cat'})
      .end(function(err, res) {
        expect(res).to.have.property('status', 400);
        expect(res.body).to.be.deep.equal({email: ['Can\'t be blank']});
        done();
      });
    });

    it('should not be able to create a user without password', function(done) {
      request(app)
      .post('/users')
      .send({email: 'userone@domain.com', password: null, firstName: 'tom', lastName: 'cat'})
      .end(function(err, res) {
        expect(res).to.have.property('status', 400);
        expect(res.body).to.be.deep.equal({password: ['Can\'t be blank']});
        done();
      });
    });

    it('should not be able to create a user without firstName', function(done) {
      request(app)
      .post('/users')
      .send({email: 'userone@domain.com', password: 'password', firstName: null, lastName: 'cat'})
      .end(function(err, res) {
        expect(res).to.have.property('status', 400);
        expect(res.body).to.be.deep.equal({firstName: ['Can\'t be blank']});
        done();
      });
    });

    it('should not be able to create a user without lastName', function(done) {
      request(app)
      .post('/users')
      .send({email: 'userone@domain.com', password: 'password', firstName: 'tom', lastName: null})
      .end(function(err, res) {
        expect(res).to.have.property('status', 400);
        expect(res.body).to.be.deep.equal({lastName: ['Can\'t be blank']});
        done();
      });
    });
  });

});
