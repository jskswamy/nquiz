'use strict';

var request = require('supertest')
  , express = require('express')
  , app = express()
  , route = require('../../../route')
  , chai = require('chai')
  , factory = require('../../factories')
  , helper = require('../../helper')
  , models = require('../../../models')
  , Question = models.Question
  , Answer = models.Answer
  , async = require('async')
  , expect = chai.expect
  , sprintf = require('sprintf');

describe('Question controller', function() {
  var getTransaction = helper.bindWebApp(this, models, app);

  describe('index', function() {

    it('should fetch all the documents without hinting the answer', function(done) {
      var transaction = getTransaction();
      helper.loginAsUser(request, app, transaction)
      .then(function(server) {
        factory.createMorethanOneQuestion(transaction).then(function(questions) {
          server.get('/questions').end(function(err, res) {
            expect(res).to.have.property('status', 200);
            done();
          });
        });
      });
    });

    it('unauthorized user should not be able to access', function(done) {
      request(app)
      .get('/questions')
      .expect(403)
      .end(function(err, res) {
        done();
      });
    });

  });

  describe('show', function() {

    it('should be able to get question with answers', function(done) {
      var transaction = getTransaction();

      factory.createQuestionWithMultipleAnswers(transaction)
      .then(function(result) {
        var question = result.question;
        var answers = result.answers;

        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.get('/questions/' + question.id)
          .end(function(err, res) {
            var data = res.body;
            expect(res).to.have.property('status', 200);
            expect(data.question).to.be.deep.equals(question.toJson());
            expect(data.answers).to.be.deep.equals(answers);
            expect(data.type).to.be.equal('cb');
            done();
          }).fail(function(err) {
            done(new Error(err));
          });
        });
      });
    });

    it('should not be able to retrive question if doesn\'t exists', function(done) {
      var transaction = getTransaction();

      helper.loginAsUser(request, app, transaction)
      .then(function(server) {
        server.get('/questions/0')
        .end(function(err, res) {
          expect(res).to.have.property('status', 404);
          done();
        });
      });
    });

    it('unauthorized user should not be able to access', function(done) {
      var transaction = getTransaction();
      factory.createQuestion(transaction)
      .then(function(question) {
        request(app)
        .get('/questions/' + question.id)
        .end(function(err, res) {
          expect(res).to.have.property('status', 403);
          done();
        });
      });
    });

  });

  describe('create', function() {
    it('unauthorized user should not be able to access', function(done) {
      request(app).post('/questions')
      .send({text: 'question'})
      .end(function(err, res) {
        expect(res).to.have.property('status', 403);
        done();
      });
    });

    it('should be able to create question with text', function(done) {
      var transaction = getTransaction();

      helper.loginAsUser(request, app, transaction)
      .then(function(server) {
        server.post('/questions')
        .send({text: 'question'})
        .end(function(err, res) {
          expect(res).to.have.property('status', 200);
          expect(res.body).to.have.property('text', 'question');
          done();
        });
      });
    });

    it('should be able to create question with text and reason', function(done) {
      var transaction = getTransaction();

      helper.loginAsUser(request, app, transaction)
      .then(function(server) {
        server.post('/questions')
        .send({text: 'question', reason: 'reason'})
        .end(function(err, res) {
          expect(res).to.have.property('status', 200);
          var question = res.body;
          expect(question).to.have.property('text', 'question');
          expect(question).to.have.property('reason', 'reason');
          done();
        });
      });
    });

    it('should not be able to create question without text', function(done) {
      var transaction = getTransaction();

      helper.loginAsUser(request, app, transaction)
      .then(function(server) {
        server.post('/questions')
        .end(function(err, res) {
          expect(res).to.have.property('status', 400);
          expect(res.body).to.be.deep.equal({text: ['Can\'t be blank']});
          done();
        });
      });
    });

    it('should be able to create question with answers', function(done) {
      var transaction = getTransaction();
      var answerParams = [chai.create('answer'), chai.create('answer')];

      helper.loginAsUser(request, app, transaction)
      .then(function(server) {
        server.post('/questions')
        .send({text: 'question with answer', answers: answerParams})
        .end(function(err, res) {
          expect(res).to.have.property('status', 200);
          var actualQuestion = res.body;
          Answer.findBy({where: {questionId: actualQuestion.id}}, {transaction: transaction})
          .then(function() {
            done();
          })
          .fail(function() {
            done(new Error('expected answers to be created along with question'));
          });
        });
      });
    });

    it('should not be able to create question with invalid answers');
  });

  describe('delete', function() {

    it('unauthorized user should not be able to access', function(done) {
      request(app)
      .delete(sprintf('/questions/%d', 0))
      .end(function(err, res) {
        expect(res).to.have.property('status', 403);
        done();
      });
    });

    it('should be able to delete question', function(done) {
      var transaction = getTransaction();
      factory.createQuestion(transaction).then(function(question) {
        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.delete(sprintf('/questions/%d', question.id))
          .end(function(err, res) {
            expect(res).to.have.property('status', 200);
            Question.findBy(question.id, {transaction: transaction})
            .then(function() {
              done(new Error('expected question to get deleted but not'));
            })
            .fail(function() {
              done();
            });
          });
        });
      });
    });

    it('should not delete if the question doesn\'t exist', function(done) {
      var transaction = getTransaction();
      helper.loginAsUser(request, app, transaction)
      .then(function(server) {
        server.delete(sprintf('/questions/%d',0))
        .end(function(err, res) {
          expect(res).to.have.property('status', 404);
          done();
        });
      });
    });
  });

  describe('update', function() {

    it('unauthorized user should not be able to access', function(done) {
      factory.createQuestion(getTransaction()).then(function(question) {
        request(app)
        .put('/questions/' + question.id)
        .send({text: 'updated question'})
        .end(function(err, res) {
          expect(res).to.have.property('status', 403);
          done();
        });
      });
    });

    it('should be able to update question', function(done) {
      var transaction = getTransaction();

      factory.createQuestion(getTransaction()).then(function(question) {
        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.put('/questions/' + question.id)
          .send({text: 'updated question', reason: 'updated reason'})
          .end(function(err, res) {
            expect(res).to.have.property('status', 200);
            Question.find(question.id, { transaction: transaction }).success(function(updatedQuestion) {
              expect(updatedQuestion.text).to.be.equal('updated question');
              expect(updatedQuestion.reason).to.be.equal('updated reason');
              done();
            });
          });
        });
      });
    });

    it('should not be able to update the question if it doesn\'t exists', function(done) {
      var transaction = getTransaction();

      helper.loginAsUser(request, app, transaction)
      .then(function(server) {
        server.put('/questions/0')
        .send({text: 'updating question'})
        .end(function(err, res) {
          expect(res).to.have.property('status', 404);
          done();
        });
      });
    });

    it('should not be able to update the question if text is empty', function(done) {
      var transaction = getTransaction();

      factory.createQuestion(getTransaction()).then(function(question) {
        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.put('/questions/' + question.id)
          .send({text: null})
          .end(function(err, res) {
            expect(res).to.have.property('status', 400);
            expect(res.body).to.be.deep.equal({text: ['Can\'t be blank']});
            done();
          });
        });
      });
    });

  });
});
