'use strict';

var assert = require('assert')
  , chai = require('chai')
  , jsonHelper = require('../../../helpers/json')
  , expect = chai.expect;

describe('json', function() {
  it('should be able to prase json', function(done) {
    var sampleJson = {name: 'tom', age: 20};
    expect(jsonHelper.parse(JSON.stringify(sampleJson))).to.be.deep.equal(sampleJson);
    done();
  });

  it('should return empty array if not able to parse', function(done) {
    expect(jsonHelper.parse('bla bla')).to.be.equal(null);
    done();
  });

});
