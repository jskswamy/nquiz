'use strict';

var assert = require('assert')
  , models = require('../../../models')
  , helper = require('../../helper')
  , factory = require('../../factories')
  , chai = require('chai')
  , bcrypt = require('bcrypt')
  , expect = chai.expect
  , User = models.User;

describe('User', function() {
  var getTransaction = helper.useTransactions(this, models);

  describe('Validations', function() {
    it('should be able to create user with email, password, firstname & lastname', function(done) {
      var transaction = getTransaction();
      var userParams = {email: 'user1@domain.com', password: 'password', firstName: 'user', lastName: 'one'};
      factory.createUser(transaction, userParams)
      .then(function(user) {
        expect(user).to.have.property('email', 'user1@domain.com');
        expect(user.verifyPassword('password')).to.be.equal(true);
        expect(user).to.have.property('firstName', 'user');
        expect(user).to.have.property('lastName', 'one');
        done();
      })
      .fail(function(err) {
        done(new Error(err));
      });
    });

    it('should not be able to create user wihout email', function(done) {
      var transaction = getTransaction();
      var userParams = {email: null, password: 'password', firstName: 'user', lastName: 'one'};
      factory.createUser(transaction, userParams)
      .then(function(user) {
        done(new Error('Able to create user without email'));
      })
      .fail(function(err) {
        expect(err.email).to.be.deep.equal(['Can\'t be blank']);
        done();
      });
    });

    it('should not be able to create user wihout password', function(done) {
      var transaction = getTransaction();
      var userParams = {email: 'user1@domain.com', password: null, firstName: 'user', lastName: 'one'};
      factory.createUser(transaction, userParams)
      .then(function(user) {
        done(new Error('Able to create user without password'));
      })
      .fail(function(err) {
        expect(err.password).to.be.deep.equal(['Can\'t be blank']);
        done();
      });
    });

    it('should not be able to create user wihout firstName', function(done) {
      var transaction = getTransaction();
      var userParams = {email: 'user1@domain.com', password: 'password', firstName: null, lastName: 'one'};
      factory.createUser(transaction, userParams)
      .then(function(user) {
        done(new Error('Able to create user without firstName'));
      })
      .fail(function(err) {
        expect(err.firstName).to.be.deep.equal(['Can\'t be blank']);
        done();
      });
    });

    it('should not be able to create user wihout lastName', function(done) {
      var transaction = getTransaction();
      var userParams = {email: 'user1@domain.com', password: 'password', firstName: 'user', lastName: null};
      factory.createUser(transaction, userParams)
      .then(function(user) {
        done(new Error('Able to create user without lastName'));
      })
      .fail(function(err) {
        expect(err.lastName).to.be.deep.equal(['Can\'t be blank']);
        done();
      });
    });
  });

  it('should be able to get user by email', function(done) {
    var transaction = getTransaction();
    factory.createUser(transaction)
    .then(function(user) {
      User.findByEmail(user.email, {transaction: transaction})
      .then(function(actualUser) {
        expect(user.toJson()).to.be.deep.equal(actualUser.toJson());
        done();
      });
    });
  });
});
