"use strict";

var assert = require('assert')
  , models = require('../../../models')
  , helper = require('../../helper')
  , extend = require('extend')
  , Question = models.Question
  , Answer = models.Answer;

describe('Answer', function() {
  var getTransaction = helper.useTransactions(this, models);

  describe('Validation', function() {
    it('Should not be able to create answer without question', function(done) {
      Answer.create({questionId: null, text: 'Some text'}, {transaction: getTransaction()}).success(function() {
        throw helper.validationException("able to create answer without questionId");
      }).error(function() {
        done();
      });
    });

    it('Should not be able to create answer without text', function(done) {
      Answer.create({questionId: 1, text: null}, {transaction: getTransaction()}).success(function() {
        throw helper.validationException("able to create answer without text");
      }).error(function() {
        done();
      });
    });

    it('Should be able to create answer with question and text', function(done) {
      Question.create({ text: 'question text' }, {transaction: getTransaction()}).success(function(question) {
        var answer = Answer.build({text: 'some text', questionId: question.id});
        answer.save({transaction: getTransaction()}).success(function() {
          done();
        }).error(function(error) {
          throw helper.validationException(error.message);
        });
      });
    });
  });
});
