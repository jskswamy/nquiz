'use strict';

var assert = require('assert')
  , models = require('../../../models')
  , helper = require('../../helper')
  , factory = require('../../factories')
  , chai = require('chai')
  , expect = chai.expect;

describe('Category', function() {
  var getTransaction = helper.useTransactions(this, models);

  it('should be able to create category with name', function(done) {
    factory.createCategory(getTransaction(), {name: 'new category'})
    .then(function() {
      done();
    })
    .fail(function() {
      done(new Error('unable to create category without name'));
    });
  });

  it('should not be able to create category without name', function(done) {
    factory.createCategory(getTransaction(), {name: null})
    .then(function() {
      done(new Error('able to create category without name'));
    })
    .fail(function(err) {
      expect(err).to.be.deep.equal({name: ["Can't be blank"]});
      done();
    });
  });
});
