'use strict';

var assert = require('assert')
  , chai = require('chai')
  , _ = require('lodash')
  , models = require('../../../models')
  , helper = require('../../helper')
  , factory = require('../../factories')
  , Question = models.Question
  , Answer = models.Answer
  , Category = models.Category
  , expect = chai.expect
  , sprintf = require('sprintf');


var getResultJson = function(questions) {
  var result = _.reduce(questions, function(result, question) {
    var answers = _.chain(question.answers)
    .filter(function(answer) {
      return answer.isAnswer;
    }).map(function(answer) {
      return answer.id;
    }).value();

    result[question.id] = {answers: answers || null};
    return result;
  }, {});

  return JSON.stringify(result);
};

describe('Question', function() {

  var getTransaction = helper.useTransactions(this, models);

  describe('Validation', function() {
    it('Should not be able question without text', function(done) {
      factory.createQuestion(getTransaction(), { text: null })
      .then(function() {
        done(new Error('able to create question without text'));
      })
      .fail(function() {
        done();
      });
    });

    it('should be able to create without reason', function(done) {
      var transaction = getTransaction();

      factory.createQuestion(transaction, chai.create('question', {reason: null}))
      .then(function(question) {
        done();
      }).fail(function() {
        done(new Error('unable to create question without reason field'));
      });

    });

    it('Should be able to create test with text', function(done) {
      factory.createQuestion(getTransaction())
      .then(function() {
        done();
      })
      .fail(function() {
        done(new Error('unable to create question with text'));
      });
    });
  });

  describe('Type', function() {
    it('question type should be choose the best', function(done) {
      var transaction = getTransaction();

      factory.createQuestionWithMultipleAnswers(transaction, {
        1: { optionText: 'option1', isAnswer: false },
        2: { optionText: 'option2', isAnswer: true },
        3: { optionText: 'option3', isAnswer: false }
      })
      .then(function(result) {
        Question.findBy({where: {id: result.question.id}, include: [Answer]}, {transaction: transaction})
        .then(function(question) {
          expect(question.type()).to.be.equal('cb');
          done();
        })
        .fail(function(err) {
          done(new Error(err));
        });
      });
    });

    it('question type should be multiple choice', function(done) {
      var transaction = getTransaction();

      factory.createQuestionWithMultipleAnswers(transaction, {
        1: { optionText: 'option1', isAnswer: true },
        2: { optionText: 'option2', isAnswer: true },
        3: { optionText: 'option3', isAnswer: false }
      })
      .then(function(result) {
        Question.findBy({where: {id: result.question.id}, include: [Answer]}, {transaction: transaction})
        .then(function(question) {
          expect(question.type()).to.be.equal('mc');
          done();
        })
        .fail(function(err) {
          done(new Error(err));
        });
      });
    });
  });

  describe('resultJson', function() {

    it('should be able to return json with correct answers', function(done) {
      var transaction = getTransaction();
      factory.createMultipleQuestionsWithAnswers(transaction, {
        1: {
          question: chai.create('question'),
          answers: {
            1: chai.create('answer', {isAnswer: true}),
            2: chai.create('answer'),
            3: chai.create('answer')
          }
        }
      }).then(function(questions) {
        expect(Question.resultJson(questions)).to.be.equal(getResultJson(questions));
        done();
      }).fail(function(err) {
        done(new Error(err));
      });
    });

    it('should be able to return json with correct multiple answers', function(done) {
      var transaction = getTransaction();
      factory.createMultipleQuestionsWithAnswers(transaction, {
        1: {
          question: chai.create('question'),
          answers: {
            1: chai.create('answer', {isAnswer: true}),
            2: chai.create('answer'),
            3: chai.create('answer', {isAnswer: true})
          }
        }
      }).then(function(questions) {
        expect(Question.resultJson(questions)).to.be.equal(getResultJson(questions));
        done();
      }).fail(function(err) {
        done(new Error(err));
      });
    });

    it('should be able to return json with correct multiple question and answers', function(done) {
      var transaction = getTransaction();
      factory.createMultipleQuestionsWithAnswers(transaction, {
        1: {
          question: chai.create('question'),
          answers: {
            1: chai.create('answer', {isAnswer: true}),
            2: chai.create('answer'),
            3: chai.create('answer', {isAnswer: true})
          }
        },
        2: {
          question: chai.create('question'),
          answers: {
            1: chai.create('answer'),
            2: chai.create('answer'),
            3: chai.create('answer', {isAnswer: true})
          }
        }
      }).then(function(questions) {
        expect(Question.resultJson(questions)).to.be.equal(getResultJson(questions));
        done();
      }).fail(function(err) {
        done(new Error(err));
      });
    });

  });

  describe('findAllWithAnswers', function() {
    it('should be able to fetch all the questions and answers with specified limit', function(done) {
      var transaction = getTransaction();
      factory.createMultipleQuestionsWithAnswers(transaction, {
        1: {
          question: chai.create('question'),
          answers: {
            1: chai.create('answer')
          }
        },
        2: {
          question: chai.create('question'),
          answers: {
            1: chai.create('answer'),
            2: chai.create('answer')
          }
        },
        3: {
          question: chai.create('question'),
          answers: {
            1: chai.create('answer')
          }
        }
      })
      .then(function(questionsWithAnswers) {
        Question.findAllWithLimit(2, transaction)
        .then(function(questions) {
          var expectedQuestions = _.map(questions, function(answers, questionId) { return questionId; });
          var actualQuestions = _.map(questionsWithAnswers, function(questionWithAnswer) { return sprintf('%s', questionWithAnswer.question.id); });

          expect(_.keys(questions)).to.have.property('length', 2);
          _.each(expectedQuestions, function(expectedQuestion) {
            expect(actualQuestions).to.include(expectedQuestion);
          });

          done();
        }).fail(function(err) {
          done(new Error(err));
        });
      }).fail(function(err) {
        done(new Error(err));
      });
    });

  });

  it('tojson should include reason', function(done) {
    var transaction = getTransaction();
    var params = chai.create('question');

    factory.createQuestion(transaction, params)
    .then(function(question) {
      var questionJson = question.toJson();
      expect(questionJson.id).to.be.not.equal(null);
      expect(questionJson).to.have.property('text', params.text);
      expect(questionJson).to.have.property('reason', params.reason);
      done();
    }).fail(function(err) {
      done(new Error(err));
    });
  });

});
