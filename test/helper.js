'use strict';

var bodyParser = require('body-parser')
  , passport = require('passport')
  , localAutentication = require('../config/passport')
  , route = require('../route')
  , userRoles = require('../config/authorization')()
  , factory = require('./factories')
  , session = require('cookie-session')
  , Q = require('q')
  , _ = require('lodash')
  , flash = require('express-flash')
  , async = require('async');

exports.UserException = function(name, message) {
  this.name = name;
  this.message = message || "UserException";
};

exports.validationException = function(name) {
  return new this.UserException(name, "validationException");
};

exports.useTruncation = function(mocha, models) {
  mocha.afterEach(function(done) {
    var toBeTruncated = _.where(models, function(model, modelName) { return 'tableName' in model; });
    async.each(toBeTruncated, function(model, callback) {
      model.destroy({},{truncate: true}).success(function(result) {
        callback(null);
      });
    }, function() {
      done();
    });
  });
};

exports.useTransactions = function(mocha, models, app) {
  var transaction;

  mocha.beforeEach(function(done) {
    models.sequelize.transaction(function(t) {
      transaction = t;
      done();
    });
  });

  mocha.afterEach(function(done) {
    transaction.rollback().success(function() {
      done();
    });
  });

  if (app) {
    app.use(function(req, res, next) {
      req.transaction = transaction;
      next();
    });
  }

  return function() {
    return transaction;
  };
};

exports.bindDefaultBodyParser = function(app) {
  app.use(bodyParser());
};

exports.bindRoutesWithUserRoles = function(app) {
  route.bind(app, userRoles);
};

exports.useAuthentication = function(app) {
  app.use(session({secret: 'node quiz app'}));
  app.use(passport.initialize());
  app.use(passport.session());
  localAutentication(passport);
};

exports.loginAsUser = function(request, app, transaction) {
  var defered = Q.defer();
  var userPassword = 'password';
  var server = request.agent(app);

  factory.createUser(transaction, userPassword)
  .then(function(user) {
    server.post('/login')
    .send({email: user.email, password: userPassword})
    .expect(302)
    .expect('Location', '/')
    .end(function(err, res) {
      if (err) {
        defered.reject(err);
      } else {
        defered.resolve(server);
      }
    });
  })
  .fail(function(err) {
    defered.reject(err);
  });
  return defered.promise;
};

exports.bindWebApp = function(mocha, models, app) {
  var getTransaction =  this.useTransactions(mocha, models, app);
  app.use(flash());
  app.set('view engine', 'jade');
  this.bindDefaultBodyParser(app);
  this.useAuthentication(app);
  this.bindRoutesWithUserRoles(app);
  return getTransaction;
};
