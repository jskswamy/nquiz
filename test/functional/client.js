'use strict';

var webdriver = require('webdriverjs');

exports.client = function() {
  var client = webdriver.remote({
    desiredCapabilities: {
      // http://code.google.com/p/selenium/wiki/DesiredCapabilities
      browserName: 'firefox',
      locationContextEnabled: true
    },
    // webdriverjs has a lot of output which is generally useless
    // However, if anything goes wrong, remove this to see more details
    logLevel: 'silent'
  });

  return client.init().windowHandleSize({width: 1024, height: 768});
};
