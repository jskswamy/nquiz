"use strict";

var chai = require("chai")
  , factories = require("chai-factories")
  , Q = require('q')
  , models = require("../models")
  , async = require('async')
  , _ = require('lodash')
  , moment = require('moment')
  , Question = models.Question
  , Answer = models.Answer
  , Category = models.Category
  , Session = models.Session
  , User = models.User
  , Assessment = models.Assessment;

chai.use(factories);

chai.factory('question', {text: 'some text', reason: 'some reason'});
chai.factory('answer', {questionId: 1, text: 'some text'});
chai.factory('category', {name: 'some category'});
chai.factory('session', {name: 'random test', noOfQuestions: 10, startDate: moment().format(), endDate: moment().add('days', 1).format(), duration: 60});
chai.factory('user', {email: 'tom@disney.com', password: 'password', firstName: 'Tom', lastName: 'cat'});
chai.factory('assessment', {email: 'tom@disney.com', firstName: 'tom', lastName: 'jerry', answers: JSON.stringify([]), sessionToken: 'abcd1234'});

exports.createQuestion = function(transaction, params) {
  var defered = Q.defer();

  Question.create(params || chai.create('question'),{transaction: transaction}).success(function(question) {
    defered.resolve(question);
  }).error(function(err) {
    defered.reject(err);
  });
  return defered.promise;
};

exports.createAnswerForAQuestion = function(transaction, question, optionText, isAnswer) {
  var defered = Q.defer();

  Answer.create({text: optionText, questionId: question.id, isAnswer: isAnswer}, {transaction: transaction}).success(function(answer) {
    defered.resolve(answer.toJson());
  }).error(function(err) {
    defered.reject(err);
  });
  return defered.promise;
};

exports.createMorethanOneQuestion = function(transaction, range) {
  var defered = Q.defer();
  var that = this;

  async.mapSeries(range || [1,2,3], function(item, callback) {
    that.createQuestion(transaction, {text: 'question' + item})
    .then(function(question) {
      callback(null, question.toJson());
    });
  }, function(err, questions) {
    if(err){
      defered.reject(err);
    } else {
      defered.resolve(questions);
    }
  });

  return defered.promise;
};

exports.createQuestionWithMultipleAnswers = function(transaction, optionParams) {
  var defered = Q.defer();
  var that = this;
  optionParams = optionParams || {};

  this.createQuestion(transaction).then(function(question) {
    async.mapSeries([1,2,3], function(item, callback){
      var optionParam = optionParams[item];
      var optionText = optionParam ? optionParam.optionText : 'option' + item;
      var isAnswer = optionParam ? optionParam.isAnswer : false;

      that.createAnswerForAQuestion(transaction, question, optionText, isAnswer).then(function(answer) {
        callback(null, answer);
      }).fail(function(err) {
        defered.reject(err);
      });
    }, function(err, answers) {
      if (err) {
        defered.reject(err);
      } else {
        defered.resolve({
          question: question,
          answers: answers
        });
      }
    });
  });

  return defered.promise;
};

exports.createMultipleQuestionsWithAnswers = function(transaction, params) {
  var defered = Q.defer()
    , questions = _.map(params, function(param, index) { return index; })
    , that = this;

  async.mapSeries(questions, function(index, questionCallback) {
    var questionParam = params[index].question
      , answerParams = params[index].answers
      , answers = _.map(answerParams, function(param, index) { return index; });

    that.createQuestion(transaction, questionParam)
    .then(function(question) {

      async.mapSeries(answers, function(index, answerCallback) {
        var answerParam = answerParams[index];
        Answer.create(_.assign(answerParam, {questionId: question.id}) ,{transaction: transaction})
        .success(function(answer) {
          answerCallback(null, answer);
        });
      }, function(err, answers) {
        questionCallback(null, {question: question, answers: answers});
      });
    });
  }, function(err, questionsWithAnswers) {
    defered.resolve(questionsWithAnswers);
  });
  return defered.promise;
};

exports.createQuestionWithAnAnswer = function(transaction) {
  var defered = Q.defer();
  var that = this;

  that.createQuestion(transaction).then(function(question) {
    that.createAnswerForAQuestion(transaction, question, 'answer for the question').then(function(answer) {
      defered.resolve({
        question: question,
        answer: answer
      });
    });
  });
  return defered.promise;
};

exports.createCategory = function(transaction, params) {
  var defered = Q.defer();

  Category.create(params || chai.create('category'), {transaction: transaction})
  .success(function(category) {
    defered.resolve(category);
  })
  .error(function(err) {
    defered.reject(err);
  });

  return defered.promise;
};

exports.createMultipleCategory = function(transaction) {
  var defered = Q.defer();
  var that = this;

  async.mapSeries([1,2,3], function(item, callback) {
    that.createCategory(transaction, {name: 'category' + item})
    .then(function(category) {
      callback(null, category.toJson());
    })
    .fail(function(err) {
      callback(err);
    });
  }, function(err, categories) {
    defered.resolve(categories);
  });
  return defered.promise;
};

exports.createQuestionAndCategory = function(transaction) {
  var defered = Q.defer();
  var that = this;

  async.series([function(callback) {
    that.createCategory(transaction)
    .then(function(category) {
      callback(null, category);
    });
  }, function(callback) {
    that.createQuestion(transaction)
    .then(function(question){
      callback(null, question);
    });
  }], function(err, results) {
    if (err) {
      defered.reject(err);
    } else {
      defered.resolve({category: results[0], question: results[1]});
    }
  });

  return defered.promise;
};

exports.getAssociatedQuestions = function(transaction, category) {
  var defered = Q.defer();

  category.reload({transaction: transaction})
  .success(function() {
    category.getQuestions({transaction: transaction})
    .success(function(questions) {
      defered.resolve(_.map(questions, function(question) { return question.toJson(); }));
    }).
    fail(function(err) {
      defered.reject(err);
    });
  })
  .fail(function(err) {
    defered.reject(err);
  });

  return defered.promise;
};

exports.createSession = function(transaction, params) {
  var defered = Q.defer();

  Session.create(params || chai.create('session'), {transaction: transaction})
  .success(function(session) {
    defered.resolve(session);
  })
  .error(function(err) {
    defered.reject(err);
  });

  return defered.promise;
};

exports.createMultipleSessions = function(transaction, params) {
  var defered = Q.defer();
  var that = this;

  async.mapSeries([1,2,3], function(index, callback) {
    var item = params ? params[index] : chai.create('session');
    that.createSession(transaction, item).then(function(session) {
      callback(null, session.toJson());
    });
  }, function(err, sessions) {
    if(err) {
      defered.reject(err);
    } else {
      defered.resolve(sessions);
    }
  });
  return defered.promise;
};

exports.createUser = function(transaction, params) {
  var defered = Q.defer();
  params = typeof(params) === 'string' ? chai.create('user', {password: params}) : params;

  User.create(params || chai.create('user'), {transaction: transaction})
  .success(function(user) {
    defered.resolve(user);
  })
  .error(function(err) {
    defered.reject(err);
  });

  return defered.promise;
};

exports.createAssessment = function(transaction, params) {
  var defered = Q.defer();
  params = params ? params : chai.create('assessment');

  Assessment.create(params || chai.create('assessment'), {transaction: transaction})
  .success(function(assessment) {
    defered.resolve(assessment);
  })
  .error(function(err) {
    defered.reject(err);
  });

  return defered.promise;
};

exports.createSessionAndAssessment = function(transaction, sessionParams, assessmentParams) {
  var defered = Q.defer();
  var that = this;

  that.createSession(transaction, sessionParams || chai.create('session'))
  .then(function(session) {
    that.createAssessment(transaction, assessmentParams || chai.create('assessment', {sessionToken: session.token}))
    .then(function(assessment) {
      defered.resolve({
        session: session,
        assessment: assessment
      });
    });
  });

  return defered.promise;
};

exports.createSampleQuestionsWithAnswers = function(transaction) {
  return this.createMultipleQuestionsWithAnswers(transaction, {
    1: {
      question: chai.create('question', {'text': 'Capital of India'}),
      answers: {
        1: chai.create('answer', {'text': 'Chennai'}),
        2: chai.create('answer', {'text': 'Mumbai'}),
        3: chai.create('answer', {'text': 'New Delhi', isAnswer: true})
      }
    },
    2: {
      question: chai.create('question', {'text': 'Longest river in India'}),
      answers: {
        1: chai.create('answer', {text: 'Kaveri'}),
        2: chai.create('answer', {text: 'Ganga', isAnswer: true}),
        3: chai.create('answer', {text: 'Yamuna'})
      }
    },
    3: {
      question: chai.create('question', {'text': 'Where is Tajmahal located'}),
      answers: {
        1: chai.create('answer', {text: 'Agra', isAnswer: true}),
        2: chai.create('answer', {text: 'Chennai'}),
        3: chai.create('answer', {text: 'Bangalore'}),
      }
    }
  });
};

exports.generateAnswerFromQuestionsWithAnswers = function(questionsWithAnswers) {
  return _.reduce(questionsWithAnswers, function(result, questionAndAnswer) {
    var question = questionAndAnswer.question
      , answers = questionAndAnswer.answers
      , answerIndex = Math.floor((Math.random() * 3));

    result[question.id] = {answers: [answers[answerIndex].id]};
    return result;
  }, {});
};

exports.getCorrectAnswerFromQuestionWithAnswers = function(questionsWithAnswers) {
  return _.reduce(questionsWithAnswers, function(result, questionAndAnswer) {
    var question = questionAndAnswer.question
      , answers = questionAndAnswer.answers
      , answerIndex = _.indexOf(answers, _.find(answers, function(answer) { return answer.isAnswer; }));

    result[question.id] = {answers: [answers[answerIndex].id]};
    return result;
  }, {});
};
