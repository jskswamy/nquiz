define(function(require) {
  var _ = require('underscore');
  var $ = require('jquery');

  return {
    flushAndAdd: function(target, items) {
      target.removeAll();
      $.each(items, function(index, item) {
        target.push(item);
      });
    },
    serializeForm: function(form) {
      return _.reduce($(form).serializeArray(), function(fields, field) {
        fields[field.name] = field.value;
        return fields;
      }, {});
    },
    clearForm: function(form) {
      $('input', $(form)).val('');
    }
  };
});
