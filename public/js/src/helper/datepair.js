'use strict';

define(function(require) {
  var $ = require('jquery')
    , timepicker = require('timepicker')
    , datepicker = require('datepicker')
    , _ = require('lodash')
    , momentHelper = require('helper/moment');

  var defaultOptions = {
    onChangeDate: $.noop
  };

  var bindDatePicker = function(container, callback) {
    $('.date', container).datepicker({
      'autoclose': true,
      keyboardNavigation: true,
      todayHighlight: true,
      todayBtn: "linked"
    }).on('changeDate', callback);
  };

  var bindTimePicker = function(container, callback) {
    $('.time', container).timepicker({
      showDuration: true,
      timeFormat: 'H:i'
    }).on('change', callback);
  };

  var DateTimePicker = function(container, options) {
    this.options = {onUpdate: $.noop};
    this.container = container;
    $.extend(this.options, options);
    bindDatePicker(container, $.proxy(this.handleDateTimeUpdate, this));
    bindTimePicker(container, $.proxy(this.handleDateTimeUpdate, this));
  };

  DateTimePicker.prototype = {
    handleDateTimeUpdate: function() {
      var elements = $('.date, .time', this.container);
      var dateTime = _.reduce(elements, function(result, element) {
        element = $(element);
        result[element.attr('name')] = element.val();
        return result;
      }, {});
      
      this.options.onUpdate.apply(this, [dateTime]);
    }
  };
  return DateTimePicker;
});
