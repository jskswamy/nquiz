'use strict';

define(function(require) {
  var moment = require('moment')
    , textUtil = require('sprintf');

  var DATEFORMAT = 'MM/DD/YYYY'
    , TIMEFORMAT = 'HH:mm'
    , DATETIMEFORMAT = textUtil.sprintf('%s %s', DATEFORMAT, TIMEFORMAT);

  function isValidDate(date) {
    var parsedDate = moment(new Date(date));
    return parsedDate.isValid();
  }

  return {
    getDate: function(value) {
      return isValidDate(value) ? moment(new Date(value)).format(DATEFORMAT) : value;
    },
    getTime: function(value) {
      return isValidDate(value) ? moment(new Date(value)).format(TIMEFORMAT) : value;
    },
    getDateTime: function(value) {
      return isValidDate(value) ? moment(new Date(value)).format(DATETIMEFORMAT) : value;
    },
    fromDateAndTime: function(date, time) {
      date = date ? date : moment().format(DATEFORMAT);
      time = time ? time : '00:00';
      return moment(new Date(textUtil.sprintf('%s %s',date, time))).add({hours: 5, minutes: 30}).zone(0).format();
    },
    mergeDateTime: function(dateTime) {
      var startDate = this.fromDateAndTime(dateTime.startDate, dateTime.startTime)
        , endDate = this.fromDateAndTime(dateTime.endDate, dateTime.endTime);

      return {startDate: startDate, endDate: endDate};
    }
  };
});
