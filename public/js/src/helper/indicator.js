'use strict';

define(function(require) {
  var $ = require('jquery')
    , busyTimer = null;

  function showBusy() {
    $('.busy').show();
  }

  $(document).ajaxStart(function() {
    busyTimer = setTimeout($.proxy(showBusy, this), 150);
  });

  $(document).ajaxStop(function() {
    clearTimeout(busyTimer);
    $('.busy').hide();
  });
});
