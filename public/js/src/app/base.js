'use strict';

define(function(require) {
  var $ = require('jquery')
    , tooltip = require('bootstrap/alert')
    , transition = require('bootstrap/transition')
    , collapse = require('bootstrap/collapse')
    , indicator = require('helper/indicator');

  setTimeout(function() {
    $(".alert").alert('close');
  }, 3000);

});
