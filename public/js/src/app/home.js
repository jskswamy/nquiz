'use strict';

define(function(require) {
  var Vue = require('vue')
    , $ = require('jquery')
    , Validator = require('helper/validator')
    , selector = '#assessment'
    , textUtil = require('sprintf');

  var validation = new Validator('form#assessment');

  var createViewModel = function(methods) {
    return new Vue({
      el: selector,
      data: {
        token: ''
      },
      methods: methods
    });
  };

  var Assessment = function() {
    var methods = {
      start: $.proxy(this.start, this)
    };
    this.viewModel = createViewModel(methods);
  };

  Assessment.prototype = {
    start: function(viewModel, evt) {
      evt.preventDefault();
      window.location = textUtil.sprintf('/%s', this.viewModel.token);
    }
  };

  return Assessment;

});
