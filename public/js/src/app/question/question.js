/* jshint strict: false, laxcomma: true */

define(function(require) {
  var $ = require('jquery')
    , Vue = require('vue')
    , Modal = require('helper/modal')
    , selector = '#question'
    , vueHelper = require('helper/vue')
    , Validator = require('helper/validator')
    , existingQuestion = require('app/question/existingQuestion')
    , newQuestion = require('app/question/newQuestion');

  var createViewModel = function(methods) {
    return new Vue({
      el: selector,
      methods: methods,
      data: {
        answers: []
      }
    });
  };

  var Question = function(options) {
    var methods = {
      toggleAnswer: $.proxy(this.toggleAnswer, this),
      updateAnswer: $.proxy(this.updateAnswer, this),
      addAnswer: $.proxy(this.addAnswer, this),
      deleteAnswer: $.proxy(this.deleteAnswer, this),
      saveQuestion: $.proxy(this.saveQuestion, this)
    };
    this.options = {};
    $.extend(this.options, options);
    this.validator = new Validator('#questionForm');
    this.viewModel = createViewModel(methods);
  };

  Question.prototype = {
    edit: function(id) {
      return this.load(existingQuestion, id);
    },
    new: function() {
      return this.load(newQuestion, null);
    },
    load: function(questionType, questionId) {
      var that = this;
      var defer = $.Deferred();

      this.questionId = questionId;
      this.question = questionType;
      this.modal = new Modal(selector);
      this.question.load(questionId).done(function(item) {
        that.viewModel.$data = $.extend({mode: that.question.type}, item);
        that.modal.show().done(function(question) {
          defer.resolve(question);
        });
      });
      return defer;
    },
    addAnswer: function() {
      var that = this;

      that.question.newAnswer(this.questionId)
      .done(function(answer) {
        that.viewModel.answers.push(answer);
      });
    },
    toggleAnswer: function(answer) {
      var that = this;

      that.question.toggleAnswer(this.questionId, answer)
      .done(function(updatedAnswer) {
        answer.$data.isAnswer = updatedAnswer.isAnswer;
      });
    },
    updateAnswer: function(answer) {
      this.question.updateAnswer(this.questionId, answer);
    },
    deleteAnswer: function(answer) {
      var that = this;

      that.question.deleteAnswer(this.questionId, answer)
      .done(function(item) {
        that.viewModel.answers.$remove(item);
      });
    },
    saveQuestion: function(question, e) {
      e.preventDefault();
      var that = this;

      that.question.save(this.questionId, question)
      .done(function(item) {
        that.modal.hide(item || that.viewModel.$data.question);
      });
    }
  };

  return Question;
});
