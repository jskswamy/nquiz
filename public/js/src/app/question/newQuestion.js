'use strict';

define(function(require) {

  var $ = require('jquery');

  return {
    type: 'New',
    newAnswer: function() {
      var defer = $.Deferred();

      defer.resolve({text: '', isAnswer: false });
      return defer;
    },
    updateAnswer: $.noop,
    toggleAnswer: function(questionId, answer) {
      var data = answer.$data;
      var defer = $.Deferred();

      data.isAnswer = !data.isAnswer;
      defer.resolve(data);
      return defer;
    },
    deleteAnswer: function(questionId, answer) {
      var defer = $.Deferred();

      defer.resolve(answer.$data);
      return defer;
    },
    load: function(questionId) {
      var defer = $.Deferred();

      defer.resolve({question: {text: '<br/>', reason: null}, answers: []});
      return defer;
    },
    save: function(questionId, question) {
      var defer = $.Deferred();
      var data = question.$data;
      $.ajax({
        method: 'POST',
        url: '/questions',
        data: {text: data.question.text, answers: data.answers}
      }).success(function(question) {
        defer.resolve(question);
      });
      return defer;
    }
  };
});
