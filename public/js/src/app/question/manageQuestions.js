/* jshint strict: false*/

define(function(require) {
  var $ = require('jquery')
    , Vue = require('vue')
    , modal = require('bootstrap/modal')
    , textUtil = require('sprintf')
    , Question = require('app/question/question');

  var createQuestionViewModel = function(methods) {
    return new Vue({
      el: '#questions',
      data: {
        title: 'Manage Questions',
        questions: [],
        ready: false
      },
      methods: methods
    });
  };

  var ManageQuestions = function(options) {
    var methods = {
      editQuestion: $.proxy(this.editQuestion,this),
      addQuestion: $.proxy(this.addQuestion, this),
      deleteQuestion: $.proxy(this.deleteQuestion, this)
    };
    this.options = {
      onEdit: $.noop
    };
    $.extend(this.options, options);
    this.viewModel = createQuestionViewModel.apply(this, [methods, options]);
    this.question = new Question();
  };

  ManageQuestions.prototype = {
    bindViewModel: function(result) {
      this.viewModel.questions = result;
      this.viewModel.ready = true;
    },
    load: function() {
      $.get('/questions').done($.proxy(this.bindViewModel, this));
    },
    editQuestion: function(item) {
      this.options.onEdit.apply(this, [item.$data]);
      this.question.edit(item.id).done(function(question) {
        item.$data.text = question.text;
      });
    },
    deleteQuestion: function(item) {
      var that = this;
      $.ajax({
        method: 'DELETE',
        url: textUtil.sprintf('/questions/%d', item.id)
      }).done(function() {
        that.viewModel.questions.$remove(item.$data);
      });
    },
    addQuestion: function() {
      var that = this;
      this.question.new().done(function(question) {
        that.viewModel.questions.push(question);
      });
    }
  };

  return ManageQuestions;
});
