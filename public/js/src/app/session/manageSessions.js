'use strict';

define(function(require) {

  var textUtil = require('sprintf')
    , $ = require('jquery')
    , Vue = require('vue')
    , Session = require('app/session/session');

  var createSessionViewModel = function(methods) {
    return new Vue({
      el: '#sessions',
      data: {
        title: 'Manage Sessions',
        sessions: [],
        ready: false
      },
      methods: methods
    });
  };

  var ManageSessions = function() {
    var methods = {
      editSession: $.proxy(this.editSession, this),
      deleteSession: $.proxy(this.deleteSession, this),
      addSession: $.proxy(this.addSession, this)
    };
    this.viewModel = createSessionViewModel(methods);
    this.session = new Session();
  };

  ManageSessions.prototype = {
    bindViewModel: function(sessions) {
      this.viewModel.sessions = sessions;
      this.viewModel.ready = true;
    },
    load: function() {
      $.get('/sessions').done($.proxy(this.bindViewModel, this));
    },
    editSession: function(item) {
      var that = this;
      that.session.edit(item.$data).done(function(session) {
        $.extend(item.$data, session);
      });
    },
    deleteSession: function(item) {
      var that = this;
      $.ajax({
        method: 'DELETE',
        url: textUtil.sprintf('/sessions/%s', item.token)
      }).success(function() {
        that.viewModel.sessions.$remove(item.$data);
      });
    },
    addSession: function() {
      var that = this;
      that.session.new().done(function(session) {
        that.viewModel.sessions.push(session);
      });
    }
  };

  return ManageSessions;
});
