'use strict';

define(function(require) {
  var $ = require('jquery')
    , textUtil = require('sprintf');

  return {
    type: 'Edit',
    load: function(sessionId) {
      var defer = $.Deferred();
      $.ajax({
        method: 'GET',
        url: textUtil.sprintf('/sessions/%s', sessionId)
      }).done(function(item) {
        defer.resolve(item);
      });
      return defer;
    },
    save: function(sessionId, session) {
      var defer = $.Deferred();
      $.ajax({
        method: 'PUT',
        url: textUtil.sprintf('/sessions/%s', sessionId),
        data: session
      }).done(function() {
        defer.resolve();
      });
      return defer;
    }
  };
});
