'use strict';

define(function() {

  var $ = require('jquery')
    , moment = require('moment');

  var newSession = function(name) {
    return {
      name: name,
      noOfQuestions: null,
      duration: 60,
      startDate: moment().format(),
      endDate: moment().add({days: 1}).format()
    };
  };

  return {
    type: 'New',
    load: function() {
      var defer = $.Deferred();
      defer.resolve(newSession(''));
      return defer;
    },
    save: function(sessionId, session) {
      var defer = $.Deferred();

      $.ajax({
        method: 'POST',
        url: '/sessions',
        data: session
      }).success(function(session) {
        defer.resolve(session);
      });
      return defer;
    }
  };
});
