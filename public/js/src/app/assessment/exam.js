'use strict';

define(function(require) {
  var Vue = require('vue')
    , _ = require('lodash')
    , textUtil = require('sprintf')
    , $ = require('jquery')
    , vueHelper = require('helper/vue')
    , selector = '#exam';

  var createExamViewModel = function(data, methods) {
    return new Vue({
      el: selector,
      data: data,
      methods: methods,
      computed: {
        currentIndex: function() {
          return this.$data.index + 1;
        },
        isFirst: function() {
          return this.$data.index === 0;
        },
        isLast: function() {
          return this.currentIndex === this.$data.noOfQuestions;
        },
        buttonText: function() {
          var isSelected = _.chain(this.question.answers)
          .some(function(answer){
            return answer.isAnswer;
          }).value();
          return isSelected ? 'Next' : 'Skip';
        }
      }
    });
  };

  var questionsCache = {};

  var fetchQuestion = function(questions, sessionToken, token, questionId) {
    var defer = $.Deferred()
      , cachedQuestion = questionsCache[questionId];

    if(cachedQuestion) {
      defer.resolve(cachedQuestion);
    }else {
      $.get(textUtil.sprintf('/%s/%s/questions/%s', sessionToken, token, questionId))
      .done(function(questionWithAnswers) {
        var answersForQuestion = questions[questionId].answers;
        _.each(questionWithAnswers.answers, function(answer) {
          answer.isAnswer = _.contains(answersForQuestion, answer.id) ? 'on' : false;
        });
        questionsCache[questionId] = questionWithAnswers;
        defer.resolve(questionWithAnswers);
      });
    }
    return defer;
  };

  var answersSoFar = function(answers) {
    var answersSoFar = _.reduce(questionsCache, function(result, question, questionId) {
      var selectedAnswers = _.chain(question.answers)
      .filter(function(answer) {
        return answer.isAnswer;
      }).map(function(answer) {
        return answer.id;
      }).value();
      result[questionId] = {answers: selectedAnswers};
      return result;
    }, {});
    return _.assign(_.clone(answers), answersSoFar);
  };

  var getStartIndex = function(questions) {
    var questionIds = _.keys(questions);
    var questionWithoutAnswer = _.reduce(questions, function(result, question, key) {
      if (_.isEmpty(question.answers)) {
        result.push(key);
      }
      return result;
    }, []);
    var index = _.indexOf(questionIds, _.first(questionWithoutAnswer));
    return index === -1 ? 0 : index;
  };

  var Exam = function(assessment) {
    var methods = {
      next: $.proxy(this.next, this),
      previous: $.proxy(this.previous, this),
      submit: $.proxy(this.submit, this),
      mark: $.proxy(this.mark, this),
      updateAnswer: $.proxy(this.updateAnswer, this)
    };
    var questionIds = _.keys(assessment.questions);
    var data = {
      noOfQuestions: questionIds.length,
      index: getStartIndex(assessment.questions),
      answersSoFar: answersSoFar(assessment.questions),
      question: {
        answers: []
      }
    };

    this.assessment = assessment;
    this.questionIds = questionIds;
    this.viewModel = createExamViewModel(data, methods);
    this.viewModel.$watch('question.answers', function() {
      this.answersSoFar = answersSoFar(assessment.questions);
    });
    this.load();
  };

  Exam.prototype = {
    load: function() {
      var defer = $.Deferred();
      var questionId = this.questionIds[this.viewModel.index]
        , sessionToken = this.assessment.sessionToken
        , token = this.assessment.token
        , that = this;

      fetchQuestion(this.viewModel.answersSoFar, sessionToken, token, questionId)
      .then(function(questionWithAnswers) {
        that.viewModel.question = _.assign({}, questionWithAnswers);
        that.viewModel.ready = true;
        defer.resolve(answersSoFar(that.assessment.questions));
      });
      return defer;
    },
    next: function() {
      this.viewModel.index += 1;
      this.load().then($.proxy(this.save, this));
    },
    previous: function() {
      this.viewModel.index -= 1;
      this.load().then($.proxy(this.save, this));
    },
    mark: function(item) {
      item.$data.isAnswer = true;
    },
    submit: function() {
    },
    save: function(answers) {
      var sessionToken = this.assessment.sessionToken
      , token = this.assessment.token;
      return $.ajax({
        method: 'PUT',
        url: textUtil.sprintf('/%s/%s', sessionToken, token),
        data: {answers: JSON.stringify(answers)}
      });
    },
    updateAnswer: function(item) {
      _.each(this.viewModel.question.answers, function(answer) {
        if(answer.type === 'radio') {
          answer.isAnswer = answer.id === item.id ? 'on' : false;
        }
      });
    }
  };

  return Exam;
});
