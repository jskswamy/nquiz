'use strict';

define(function(require) {
  var textUtil = require('sprintf')
    , $ = require('jquery')
    , Exam = require('app/assessment/exam');

  var Assessment = function(options) {
    var sessionToken = $('#sessionToken').val()
      , token = $('#token').val();

    $(function() {
      var url = textUtil.sprintf('/%s/%s/questions', sessionToken, token);
      $.get(url).done(function(questions) {
        var exam = new Exam({
          sessionToken: sessionToken,
          token: token,
          questions: questions
        });
      });
    });
  };

  return Assessment;
});
