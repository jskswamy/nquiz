'use strict';

define(function(require){
  var $ = require('jquery')
    , textUtil = require('sprintf')
    , Result = require('app/assessment/result');

  var AssessmentResult = function() {
    var sessionToken = $('#sessionToken').val()
      , token = $('#token').val();

    $(function() {
      var url = textUtil.sprintf('/%s/%s/summary', sessionToken, token);
      $.get(url).done(function(result) {
        var exam = new Result({
          sessionToken: sessionToken,
          token: token,
          result: result
        });
      });
    });
  };

  return AssessmentResult;
});
