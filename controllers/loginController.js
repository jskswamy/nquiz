'use strict';

exports.form = function(req, res) {
  res.render('login', {title: 'Login'});
};

exports.logout = function(req, res) {
  req.flash('info', 'Successfully logged out');
  req.logout();
  res.redirect('/');
};
