'use strict';

var models = require("../models")
  , sprintf = require('sprintf')
  , jsonHelper = require('../helpers/json')
  , Q = require('q')
  , Session = models.Session
  , Question = models.Question
  , Answer = models.Answer
  , Assessment = models.Assessment
  , _ = require('lodash');

var updateAnswers = function(sessionToken, token, params, transaction) {
  var condition = {token: token, sessionToken: sessionToken}
    , defered = Q.defer();

  Assessment.findBy({where: condition}, {transaction: transaction})
  .then(function(assessment) {
    assessment.updateAttributes(params, {transaction: transaction})
    .success(function() {
      defered.resolve(assessment);
    }).fail(function(err) {
      defered.reject({code: 403, err: err});
    });
  }).fail(function(err) {
    defered.reject({code: 404});
  });

  return defered.promise;
};

var getQuestionIdsFromAnswers = function(answers) {
  return _.chain(jsonHelper.parse(answers)).keys().map(function(key) { return key*1; }).value();
};

exports.new = function(req, res) {
  var token = req.param('token');
  Session.findBy({where: {token: token}}, {transaction: req.transaction})
  .then(function(session) {
    res.render('new-assessment', { title: 'Begin assessment', token: token });
  })
  .fail(function(err) {
    res.send(404);
  });
};

exports.create = function(req, res) {
  var token = req.param('token');
  Session.findBy({where: {token: token}}, {transaction: req.transaction})
  .then(function(session) {
    var params = {email: req.param('email'), firstName: req.param('firstName'), lastName: req.param('lastName'), sessionToken: session.token};

    Assessment.findBy({where: {email: params.email, sessionToken: params.sessionToken}}, {transaction: req.transaction})
    .then(function(assessment) {
      res.redirect(sprintf('/%s/%s',session.token, assessment.token));
    })
    .fail(function() {
      Assessment.create(params, {transaction: req.transaction})
      .success(function(assessment) {
        res.redirect(sprintf('/%s/%s',session.token, assessment.token));
      })
      .fail(function(err) {
        res.send(400, err);
      });
    });
  })
  .fail(function(err) {
    res.send(404);
  });
};

exports.update = function(req, res) {
  var token = req.param('token')
    , sessionToken = req.param('sessionToken')
    , answers = req.param('answers');

  updateAnswers(sessionToken, token, {answers: answers}, req.transaction)
  .then(function() {
    res.send(200);
  }).fail(function(err) {
    res.send(err.code, err.err);
  });
};

exports.index = function(req, res) {
  var token = req.param('token')
    , sessionToken = req.param('sessionToken');

  Assessment.findBy({where: {token: token, sessionToken: sessionToken}}, {transaction: req.transaction})
  .then(function(assessment) {
    var params = {assessment: assessment.toJson(), sessionToken: sessionToken, token: token};
    if(assessment.result) {
      params = {view: 'result-assessment', data: _.assign({title: 'Assignment', score: assessment.getScore()}, params)};
    } else {
      params = {view: 'assessment', data: _.assign({title: 'Assignment'}, params)};
    }
    res.render(params.view, params.data);
  })
  .fail(function(err) {
    res.send(404);
  });
};

exports.questions = function(req, res) {
  var token = req.param('token')
    , sessionToken = req.param('sessionToken')
    , condition = {where: {token: token, sessionToken: sessionToken}, include: [Session]};

  Assessment.findBy(condition, {transaction: req.transaction})
  .then(function(assessment) {
    Question.findAllWithLimit(assessment.session.noOfQuestions, req.transaction)
    .then(function(questions) {
      var answersSoFar = JSON.parse(assessment.answers);
      res.send(200, _.assign(questions, answersSoFar));
    });
  }).fail(function(err) {
    res.send(404);
  });
};

exports.question = function(req, res) {
  var questionId = req.param('questionId');

  Question.findBy({where: {id: questionId}, include: [Answer]}, {transaction: req.transaction})
  .then(function(question) {
    var inputType = question.type() === 'cb' ? 'radio' : 'checkbox';
    res.send(200, {text: question.text, answers: _.map(question.answers, function(answer) {
      return {text: answer.text, type: inputType, id: answer.id, isAnswer: false};
    })});
  }).fail(function(err) {
    res.send(404);
  });
};

exports.result = function(req, res) {
  var token = req.param('token')
    , sessionToken = req.param('sessionToken')
    , answers = req.param('answers')
    , questionIds = getQuestionIdsFromAnswers(answers);

  Question.findAll({where: {id: questionIds}, include: [Answer]}, {transaction: req.transaction})
  .then(function(questions) {
    var result = Question.resultJson(questions);
    updateAnswers(sessionToken, token, {answers: answers, result: result}, req.transaction)
    .then(function(assessment) {
      res.redirect(sprintf('/%s/%s', sessionToken, token));
    }).fail(function(err) {
      res.send(err.code, err.err);
    });
  });
};

var getAnswerInfo = function(answer, isCorrect) {
  var isSelected = _.contains(this[answer.questionId].answers, answer.id)
    , isAnswer = answer.isAnswer;

  return {
    text: answer.text,
    type: isSelected ? isSelected && isAnswer ? 'correct' : 'wrong' :  isAnswer ? 'correct' : 'not-selected'
  };
};

var getQuestionInfo = function(question) {
  return {
    text: question.text,
    answers: _.map(question.answers, getAnswerInfo.bind(this)),
    reason: question.reason
  };
};

exports.summary = function(req, res) {
  var token = req.param('token')
    , sessionToken = req.param('sessionToken');

  Assessment.findBy({where: {token: token, sessionToken: sessionToken}}, {transaction: req.transaction})
  .then(function(assessment) {
    var result = assessment.result
      , userAnswers = JSON.parse(assessment.answers)
      , questionIds = getQuestionIdsFromAnswers(result);

    Question.findAll({where: {id: questionIds}, include: [Answer]}, {transaction: req.transaction})
    .then(function(questions) {
      var data = _.map(questions, getQuestionInfo.bind(userAnswers));
      res.send(200, data);
    });
  });
};
