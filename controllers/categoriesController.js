'use strict';

var models = require('../models')
  , _ = require('lodash')
  , Category = models.Category
  , Question = models.Question;

exports.create = function(req, res) {
  Category.create({name: req.param('name')}, {transaction: req.transaction})
  .success(function(category) {
    res.send(201, category);
  })
  .error(function(err) {
    res.send(400, err);
  });
};

exports.index = function(req, res) {
  Category.findAll({transaction: req.transaction})
  .success(function(categories) {
    res.send(200, _.map(categories, function(category){ return category.toJson(); }));
  })
  .error(function(err) {
    res.send(400, err);
  });
};

exports.update = function(req, res) {
  Category.findBy(req.param('id'), {transaction: req.transaction})
  .then(function(category) {
    Category.update({name: req.param('name')}, {id: category.id}, {transaction: req.transaction})
    .success(function() {
      res.send(200);
    })
    .fail(function(err) {
      res.send(400, err);
    });
  }).fail(function(err) {
    res.send(404, err);
  });
};

exports.addQuestion = function(req, res) {
  var questionId = req.param('questionId');
  var id = req.param('id');

  Category.findBy(id, {transaction: req.transaction})
  .then(function(category) {
    Question.findBy(questionId, {transaction: req.transaction})
    .then(function(question) {
      category.addQuestion(question, {transaction: req.transaction})
      .success(function() {
        res.send(200);
      });
    })
    .fail(function(err) {
      res.send(404, err);
    });
  })
  .fail(function(err) {
    res.send(404, err);
  });
};
