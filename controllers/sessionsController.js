'use strict';

var models = require('../models')
  , _ = require('lodash')
  , Session = models.Session
  , Category = models.Category
  , moment = require('moment');

var convertToGmt = function(dateTime) {
  return dateTime ? moment(new Date(dateTime)).zone('-  5:30').format() : null;
};

exports.index = function(req, res) {
  Session.findAll({transaction: req.transaction})
  .success(function(sessions) {
    res.send(200, _.map(sessions, function(session) { return session.toJson(); }));
  })
  .fail(function(err) {
    res.send(400, err);
  });
};

exports.show = function(req, res) {
  Session.findBy({where: {token : req.param('token')}}, {transaction: req.transaction})
  .then(function(session) {
    res.send(200, session.toJson());
  })
  .fail(function(session) {
    res.send(404);
  });
};

exports.create = function(req, res) {
  Session.create({
    name: req.param('name'),
    noOfQuestions: req.param('noOfQuestions'),
    startDate: convertToGmt(req.param('startDate')) || null,
    endDate: convertToGmt(req.param('endDate')) || null,
    duration: req.param('duration'),
  }, {
    transaction: req.transaction
  }).success(function(session) {
    res.send(200, session.toJson());
  }).error(function(err) {
    res.send(400, err);
  });
};

exports.update = function(req, res) {
  Session.findBy({where: {token : req.param('token')}}, {transaction: req.transaction})
  .then(function(session) {
    Session.update({
      startDate: convertToGmt(req.param('startDate')) || null,
      endDate: convertToGmt(req.param('endDate')) || null,
      duration: req.param('duration'),
      name: req.param('name'),
      noOfQuestions: req.param('noOfQuestions')
    }, { token: session.token}, {transaction: req.transaction})
    .success(function() {
      res.send(200);
    })
    .error(function(err) {
      res.send(400, err);
    });
  })
  .fail(function(err) {
    res.send(404);
  });
};

exports.manage = function(req, res) {
  res.render('sessions', { title: 'Manage Sessions' } );
};

exports.delete = function(req, res) {
  var condition = {token: req.param('token')};

  Session.findBy(condition, {transaction: req.transaction})
  .then(function(session) {
    session.destroy({transaction: req.transaction})
    .success(function() {
      res.send(200);
    });
  })
  .fail(function() {
    res.send(404);
  });
};
