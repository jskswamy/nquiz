"use strict";

var models = require("../models")
  , Question = models.Question
  , Answer = models.Answer;

exports.create = function(req, res) {
  Question.findBy(req.param('id'), {transaction: req.transaction})
  .then(function(question) {
    Answer.create({questionId: req.param('id'), text: req.param('text')}, { transaction: req.transaction })
    .success(function(answer) {
      res.send(200, answer.toJson());
    }).error(function(err) {
      res.send(400, err);
    });
  })
  .fail(function() {
    res.send(404);
  });
};

exports.update = function(req, res) {
  Answer.findBy({where: { id: req.param('answerId'), questionId: req.param('id')}}, {transaction: req.transaction})
  .then(function(answer) {
    Answer.update({text: req.param('text')}, { id: answer.id, questionId: req.param('id')}, {transaction: req.transaction})
    .success(function() {
      res.send(200);
    }).error(function(err) {
      res.send(400, err);
    });
  })
  .fail(function() {
    res.send(404);
  });
};

exports.mark = function(req, res) {
  Answer.findBy({where: { id: req.param('answerId'), questionId: req.param('id') }}, { transaction: req.transaction })
  .then(function(answer){
    answer.isAnswer = !answer.isAnswer;
    Answer.update({ isAnswer: answer.isAnswer }, { id: answer.id, questionId: req.param('id') }, { transaction: req.transaction })
    .success(function() {
      res.send(200, answer.toJson());
    });
  })
  .fail(function() {
    res.send(404);
  });
};


exports.delete = function(req, res) {
  var condition = { id: req.param('answerId'), questionId: req.param('id') };
  Answer.findBy({where: condition}, { transaction: req.transaction })
  .then(function(answer) {
    Answer.destroy(condition, { transaction: req.transaction }).success(function() {
      res.send(200);
    });
  })
  .fail(function() {
    res.send(404);
  });
};
