'use strict';

var routes = require('./controllers/applicationController')
  , passport = require('passport')
  , question = require('./controllers/questionsController')
  , answer = require('./controllers/answersController')
  , category = require('./controllers/categoriesController')
  , session = require('./controllers/sessionsController')
  , user = require('./controllers/usersController')
  , assessment = require('./controllers/assessmentsController')
  , login = require('./controllers/loginController');

exports.bind = function(app, userRoles) {
  app.get('/', routes.index);

  app.get('/login', login.form);
  app.get('/logout', login.logout);

  app.post('/login', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login'
  }));

  app.post('/users', user.create);

  app.get('/questions', userRoles.is('admin'), question.index);
  app.get('/questions/manage', userRoles.is('admin'), question.manage);
  app.post('/questions', userRoles.is('admin'), question.create);
  app.get('/questions/:id', userRoles.is('admin'), question.show);
  app.put('/questions/:id', userRoles.is('admin'), question.update);
  app.delete('/questions/:id', userRoles.is('admin'), question.delete);

  app.post('/questions/:id/answers', userRoles.is('admin'), answer.create);
  app.put('/questions/:id/answers/:answerId', userRoles.is('admin'), answer.update);
  app.post('/questions/:id/answers/:answerId/mark', answer.mark);
  app.delete('/questions/:id/answers/:answerId', userRoles.is('admin'), answer.delete);

  app.get('/categories', userRoles.is('admin'), category.index);
  app.post('/categories', userRoles.is('admin'), category.create);
  app.put('/categories/:id', userRoles.is('admin'), category.update);
  app.post('/categories/:id', userRoles.is('admin'), category.addQuestion);

  app.get('/sessions', userRoles.is('admin'), session.index);
  app.get('/sessions/manage', userRoles.is('admin'), session.manage);
  app.get('/sessions/:token', userRoles.is('admin'), session.show);
  app.post('/sessions', userRoles.is('admin'), session.create);
  app.put('/sessions/:token', userRoles.is('admin'), session.update);
  app.delete('/sessions/:token', userRoles.is('admin'), session.delete);

  app.get('/:token', assessment.new);
  app.post('/:token', assessment.create);
  app.get('/:sessionToken/:token', assessment.index);
  app.put('/:sessionToken/:token', assessment.update);
  app.get('/:sessionToken/:token/summary', assessment.summary);
  app.post('/:sessionToken/:token/result', assessment.result);
  app.get('/:sessionToken/:token/questions', assessment.questions);
  app.get('/:sessionToken/:token/questions/:questionId', assessment.question);
};
