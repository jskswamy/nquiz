"use strict";

var mask = require('json-mask')
  , Q = require('q')
  , _ = require('lodash');

module.exports = function(sequelize, DataTypes) {
  var Question = sequelize.define('Question', {
    text: {
      type: DataTypes.STRING,
      validate: {
        notNull: {
          args: false,
          msg: "Can't be blank"
        }
      }
    },
    reason: {
      type: DataTypes.TEXT
    }
  },{
    timestamps: false,
    tableName: 'questions',
    instanceMethods: {
      toJson: function() {
        return mask(this, 'id,text,reason');
      },
      type: function() {
        return _.filter(this.answers, function(answer) { return answer.isAnswer; }).length > 1 ? 'mc' : 'cb';
      }
    },
    classMethods: {
      associate: function(models) {
        Question.hasMany(models.Answer);
        Question.hasMany(models.Category, {through: 'categoryQuestions'});
      },
      findAllWithLimit: function(limit, transaction) {
        var defered = Q.defer();
        Question.findAll({
          limit: limit,
          transaction: transaction
        }).success(function(questions) {
          defered.resolve(_.reduce(questions, function(result, question) {
            result[question.id] = {answers: []};
            return result;
          }, {}));
        }).fail(function(err) {
          defered.reject(err);
        });
        return defered.promise;
      },
      resultJson: function(questions) {
        var result = _.reduce(questions, function(result, question) {
          var answers = _.chain(question.answers)
          .filter(function(answer) {
            return answer.isAnswer;
          }).map(function(answer) {
            return answer.id;
          }).value();

          result[question.id] = {answers: answers || null};
          return result;
        }, {});
        return JSON.stringify(result);
      },
    }
  });

  return Question;
};
