'use strict';

var mask = require('json-mask')
  , uniqueId = require('../helpers/uniqueId')
  , notNullValidation = {
    notNull: {
      args: false,
      msg: "Can't be blank"
    }
  };

module.exports = function(sequelize, DataTypes) {
  var Session = sequelize.define('Session', {
    name: {
      type: DataTypes.STRING,
      validate: notNullValidation
    },
    startDate: {
      type: DataTypes.DATE,
      validate: notNullValidation
    },
    endDate: {
      type: DataTypes.DATE,
      validate: notNullValidation
    },
    duration: {
      type: DataTypes.INTEGER,
      validate: notNullValidation
    },
    noOfQuestions: {
      type: DataTypes.INTEGER,
      validate: notNullValidation
    },
    token: {
      type: DataTypes.STRING,
      primaryKey: true
    }
  },
  {
    timestamps: false,
    tableName: 'sessions',
    classMethods: {
      associate: function(models) {
        Session.hasMany(models.Category, {through: 'sessionCategories'});
      }
    },
    instanceMethods: {
      toJson: function() {
        return mask(this, 'startDate,endDate,duration,name,noOfQuestions,token');
      }
    },
    hooks: {
      beforeCreate: function(session, fn) {
        session.token = uniqueId();
        fn();
      }
    }
  });

  return Session;
};
