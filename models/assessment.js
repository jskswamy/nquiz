'use strict';

var mask = require('json-mask')
  , _ = require('lodash')
  , uniqueId = require('../helpers/uniqueId')
  , jsonHelper = require('../helpers/json')
  , compare = require('../helpers/compare')
  , notNullValidation = {
    notNull: {
      args: false,
      msg: "Can't be blank"
    }
  };

module.exports = function(sequelize, DataTypes) {
  var Assessment = sequelize.define('Assessment', {
    email: {
      type: DataTypes.STRING,
      validate: notNullValidation
    },
    firstName: {
      type: DataTypes.STRING,
      validate: notNullValidation
    },
    lastName: {
      type: DataTypes.STRING,
      validate: notNullValidation
    },
    sessionToken: {
      type: DataTypes.STRING,
      validate: notNullValidation
    },
    answers: {
      type: DataTypes.TEXT
    },
    token: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    result: {
      type: DataTypes.TEXT
    }
  },
  {
    timestamps: false,
    tableName: 'assessments',
    classMethods: {
      associate: function(models) {
        Assessment.belongsTo(models.Session, {foreignKey: 'sessionToken'});
      }
    },
    instanceMethods: {
      toJson: function() {
        return mask(this, 'token,email,firstName,lastName,answers');
      },
      getScore: function() {
        var answers = jsonHelper.parse(this.answers)
          , exam = jsonHelper.parse(this.result);

        return _.chain(answers).filter(function(answer, key) {
          return compare.hash(answer, exam[key]);
        }).size().value();
      }
    },
    hooks: {
      beforeCreate: function(assessment, fn) {
        assessment.token = uniqueId();
        fn();
      }
    }
  });

  return Assessment;
};
