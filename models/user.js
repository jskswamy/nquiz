'use strict';

var mask = require('json-mask')
  , bcrypt = require('bcrypt');

module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define('User', {
    email: {
      type: DataTypes.STRING,
      validate: {
        notNull: {
          args: false,
          msg: "Can't be blank"
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      set: function(value) {
        if(value) {
          this.dataValues.password = bcrypt.hashSync(value, 8);
        }
      },
      validate: {
        notNull: {
          args: false,
          msg: "Can't be blank"
        }
      }
    },
    firstName: {
      type: DataTypes.STRING,
      validate: {
        notNull: {
          args: false,
          msg: "Can't be blank"
        }
      }
    },
    lastName: {
      type: DataTypes.STRING,
      validate: {
        notNull: {
          args: false,
          msg: "Can't be blank"
        }
      }
    }
  }, {
    timestamps: false,
    tableName: 'users',
    classMethods: {
      findByEmail: function(email, params) {
        return User.findBy({where: {email: email}}, params);
      }
    },
    instanceMethods: {
      toJson: function() {
        return mask(this, 'id,email,firstName,lastName');
      },
      verifyPassword: function(value) {
        return bcrypt.compareSync(value, this.password);
      }
    }
  });

  return User;
};
