'use strict';

var extend = require('extend')
  , Q = require('q');

var extensions = {
  findBy: function(criteria, options) {
    var defered = Q.defer();
    var that = this;
    this.find(criteria, options).success(function(item) {
      if (item) {
        defered.resolve(item);
      } else {
        defered.reject(new Error("Unable to find " + that.name + " with criteria " + criteria));
      }
    });
    return defered.promise;
  }
};

module.exports = function(model) {
  extend(true, model, extensions);
};
